
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Filip
 */
public class Board {

    private int[][] blocks;
    private final int size;

    public Board(int[][] blocks) // construct a board from an n-by-n array of blocks
    // (where blocks[i][j] = block in row i, column j)
    {
        size = blocks[0].length;
        for (int s = 0; s < size; ++s) {
            this.blocks[s] = Arrays.copyOf(blocks[s], size);
        }
        // this.blocks = blocks;

    }

    public int dimension() // board dimension n
    {
        return blocks[0].length;
    }

    public int hamming() // number of blocks out of place
    {
        int counter = 0;
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                if (blocks[i][j] != i * size + j + 1 && i * size + j + 1 != size * size) {
                    ++counter;
                }
            }
        }
        return counter;
    }

    public int manhattan() // sum of Manhattan distances between blocks and goal
    {
        int counter = 0;
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                if (blocks[i][j] != 0 && blocks[i][j] != i * size + j + 1) {
                    if ((blocks[i][j] - 1) / size > i) {
                        counter += (blocks[i][j] - 1) / size - i;
                    } else {
                        counter += -(blocks[i][j] - 1) / size + i;
                    }
                    if ((blocks[i][j] - 1) % size > j) {
                        counter += (blocks[i][j] - 1) % size - j;
                    } else {
                        counter += -(blocks[i][j] - 1) % size + j;
                    }
                    // counter += max((blocks[i][j] - 1) / size, i) - min((blocks[i][j] - 1) / size, i) + max((blocks[i][j] - 1) % size, j) - min((blocks[i][j] - 1) % size, j);
                }
            }
        }
        return counter;
    }

    public boolean isGoal() // is this board the goal board?
    {
        return hamming() == 0;
    }

    public Board twin() // a board that is obtained by exchanging any pair of blocks
    {
        int[][] tempBoard = new int[size][size];
        for (int s = 0; s < size; ++s) {
            tempBoard[s] = Arrays.copyOf(blocks[s], size);
        }
        Board toReturn;
        toReturn = new Board(tempBoard);
        toReturn.exchangeRandom();
        return toReturn;
    }

    private void exchangeRandom() {
        int i = 0, j = 0, k = 0, m = 0, temp;
        while ((blocks[i = StdRandom.uniform(0, size)][j = StdRandom.uniform(0, size)] == 0
                || blocks[k = StdRandom.uniform(0, size)][m = StdRandom.uniform(0, size)] == 0) || (i == k && j == m)) {;
        }
        temp = blocks[i][j];
        blocks[i][j] = blocks[k][m];
        blocks[k][m] = temp;
    }

    @Override
    public boolean equals(Object y) // does this board equal y?
    {
        if (blocks == null || y == null || dimension() != ((Board) y).dimension()) {
            return false;
        }
        return toString().equals(((Board) y).toString());
    }

    private Board swap(int i, int j, int k, int m) // i,j indicies of 0
    {
        int[][] tempBoard = new int[size][size];
        for (int s = 0; s < size; ++s) {
            tempBoard[s] = Arrays.copyOf(blocks[s], size);
        }
        tempBoard[i][j] = tempBoard[k][m];
        tempBoard[k][m] = 0;
        return new Board(tempBoard);
    }

    public Iterable<Board> neighbors() // all neighboring boards
    {
        Stack<Board> neighborBoards = new Stack<Board>();
        initNeighbors(neighborBoards);
        return neighborBoards;
    }

    private void initNeighbors(Stack<Board> neighborBoards) {
        int i = 0, x, y;
        while (blocks[i / size][i % size] != 0) {
            ++i;
        }

        x = i % size;
        y = i / size;
        if (y == 0) {
            if (x == 0) {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y + 1, x));
            } else if (x == size - 1) {
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y + 1, x));
            } else {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y + 1, x));
            }
        } else if (y == size - 1) {
            if (x == 0) {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y - 1, x));
            } else if (x == size - 1) {
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y - 1, x));
            } else {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y - 1, x));
            }
        } else {
            if (x == 0) {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y - 1, x));
                neighborBoards.push(swap(y, x, y + 1, x));
            } else if (x == size - 1) {
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y - 1, x));
                neighborBoards.push(swap(y, x, y + 1, x));
            } else {
                neighborBoards.push(swap(y, x, y, x + 1));
                neighborBoards.push(swap(y, x, y, x - 1));
                neighborBoards.push(swap(y, x, y - 1, x));
                neighborBoards.push(swap(y, x, y + 1, x));
            }
        }
    }

    @Override
    public String toString() // string representation of this board (in the output format specified below)
    {
        StringBuilder s = new StringBuilder();
        s.append(size + "\n");
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                s.append(String.format("%2d ", blocks[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void main(String[] args) // unit tests (not graded)
    {
        int[][] table = {{2, 4, 8}, {0, 1, 7}, {6, 3, 5}};// {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 0, 15}};
        Board b = new Board(table);

        StdOut.println(b.twin().toString());
        //  assert b.isGoal() == false;
//        StdOut.print(b.toString());
//        Iterable<Board> i = b.neighbors();
//        for (Board x : i) {
//            StdOut.println(x.toString());
//        }
    }
}
