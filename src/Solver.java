
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Filip
 */
public class Solver {

    private int moves = 0;
    private Node searchNodeMain, searchNodeTwin;
    private Stack<Board> shortestPath;

    public Solver(Board initial) // find a solution to the initial board (using the A* algorithm)
    {
        if (initial == null) {
            throw new IllegalArgumentException();
        }
        MinPQ<Node> mainPQ = new MinPQ<>();
        MinPQ<Node> twinPQ = new MinPQ<>();
        searchNodeMain = new Node(initial, moves);
        searchNodeTwin = new Node(initial.twin(), moves);
        for (Board neighbor : searchNodeMain.currentBoard().neighbors()) {
            mainPQ.insert(new Node(neighbor, searchNodeMain.moves() + 1, searchNodeMain));
        }
        for (Board neighbor : searchNodeTwin.currentBoard().neighbors()) {
            twinPQ.insert(new Node(neighbor, searchNodeTwin.moves() + 1, searchNodeTwin));
        }
        searchNodeMain = mainPQ.delMin();
        searchNodeTwin = twinPQ.delMin();
        while (searchNodeMain.currentBoard().isGoal() != true && searchNodeTwin.currentBoard().isGoal() != true) {
            for (Board neighbor : searchNodeMain.currentBoard().neighbors()) {
                if (searchNodeMain.previousNode().currentBoard().equals(neighbor) == false) {
                    mainPQ.insert(new Node(neighbor, searchNodeMain.moves() + 1, searchNodeMain));
                }
            }
            for (Board neighbor : searchNodeTwin.currentBoard().neighbors()) {
                if (searchNodeTwin.previousNode().currentBoard().equals(neighbor) == false) {
                    twinPQ.insert(new Node(neighbor, searchNodeTwin.moves() + 1, searchNodeTwin));
                }
            }

            searchNodeMain = mainPQ.delMin();
            searchNodeTwin = twinPQ.delMin();

        }
        moves = searchNodeMain.moves();

    }

    public boolean isSolvable() // is the initial board solvable?
    {
        return !searchNodeTwin.currentBoard().isGoal();
    }

    public int moves() // min number of moves to solve initial board; -1 if unsolvable
    {
        if (!isSolvable()) {
            return -1;
        }
        return moves;
    }

    public Iterable<Board> solution() // sequence of boards in a shortest solution; null if unsolvable
    {
        if (!isSolvable()) {
            return null;
        }
        if (shortestPath == null) {
            shortestPath = new Stack<>();
            while (searchNodeMain != null) {
                shortestPath.push(searchNodeMain.currentBoard());
                searchNodeMain = searchNodeMain.previousNode();
            }
        }
        return shortestPath;
    }

    private class Node implements Comparable<Node> {

        final private Board currentBoard;
        final private Node previousNode;
        private final int moves, manhattan;

        public Node(Board board, int moves, Node previousNode) {
            this.currentBoard = board;
            this.moves = moves;
            this.manhattan = board.manhattan();
            this.previousNode = previousNode;
        }

        public Node(Board board, int moves) {
            this.currentBoard = board;
            this.moves = moves;
            this.manhattan = board.manhattan();
            this.previousNode = null;
        }

        public Board currentBoard() {
            return currentBoard;
        }

        @Override
        public int compareTo(Node t) {
            if (this.priority() > t.priority()) {
                return 1;
            } else if (this.priority() < t.priority()) {
                return - 1;
            } else {
                return 0;
            }
        }

        public int moves() {
            return moves;
        }

        public int priority() {
            return moves + manhattan;
        }

        public Node previousNode() {
            return previousNode;
        }

    }

    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable()) {
            StdOut.println("No solution possible");
        } else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution()) {
                StdOut.println(board);
            }
        }
    }
}
